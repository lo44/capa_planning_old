defmodule CapaPlanningWeb.PageControllerTest do
  use CapaPlanningWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Hello Test angular!"
  end
end
