const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const rxPaths = require('rxjs/_esm5/path-mapping');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const { NoEmitOnErrorsPlugin, SourceMapDevToolPlugin, EvalDevToolModulePlugin, NamedModulesPlugin } = require('webpack');
const { NamedLazyChunksWebpackPlugin, BaseHrefWebpackPlugin } = require('@angular/cli/plugins/webpack');
const { CommonsChunkPlugin } = require('webpack').optimize;
const { AngularCompilerPlugin } = require('@ngtools/webpack');

const nodeModules = path.join(process.cwd(), 'node_modules');
const realNodeModules = fs.realpathSync(nodeModules);
const genDirNodeModules = path.join(process.cwd(), 'src', '$$_gendir', 'node_modules');
const minimizeCss = false;

module.exports = {

  resolve: {
    "extensions": [
      ".ts",
      ".js"
    ],
    modules: [
      "./node_modules"
    ],
    symlinks: true,
    alias: rxPaths(),
    mainFields: [
      "browser",
      "module",
      "main"
    ]
  },
  resolveLoader: {
    modules: [
      "./node_modules"
    ],
    alias: rxPaths()
  },
  entry: {
    main: [
      "./src/main.ts"
    ],
    polyfills: [
      "./src/polyfills.ts"
    ],
    styles: [
      "./vendor/bootstrap.min.css",
      "./src/styles.css"
    ]
  },
  watchOptions: {
    aggregateTimeout: 3000
  },
  output: {
    path: path.join(process.cwd(), '..', '..', 'priv', 'static', 'dist'),
    filename: "[name].bundle.js",
    chunkFilename: "[id].chunk.js",
    crossOriginLoading: false
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: "html-loader"
      },
      {
        test: /\.(eot|svg|cur)$/,
        loader: "file-loader",
        options: {
          name: "[name].[hash:20].[ext]",
          limit: 10000
        }
      },
      {
        test: /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani)$/,
        loader: "url-loader",
        options: {
          name: "[name].[hash:20].[ext]",
          limit: 10000
        }
      },
      // CSS
      {
        exclude: [
          path.join(process.cwd(), "vendor/bootstrap.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        test: /\.css$/,
        use: [
          "exports-loader?module.exports.toString()",
          {
            loader: "css-loader",
            options: {
              sourceMap: false,
              importLoaders: 1
            }
          }
        ]
      },
      {
        include: [
          path.join(process.cwd(), "vendor/bootstrap.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        })
      },
      // TS
      {
        test: /\.ts$/,
        exclude: [/\.(spec|e2e)\.ts$/],
        loaders: [{
          loader: 'awesome-typescript-loader',
          options: { configFileName: path.join(process.cwd(), 'tsconfig.json') }
        }, 'angular2-template-loader']
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)@angular/,
      path.join(process.cwd(), 'src'), // location of your src
      {} // a map of your routes
    ),
    new NoEmitOnErrorsPlugin(),
    new ProgressPlugin(),
    new CircularDependencyPlugin({
      exclude: /(\\|\/)node_modules(\\|\/)/,
      failOnError: false,
      onDetected: false,
      cwd: process.cwd()
    }),
    new NamedLazyChunksWebpackPlugin(),
    new BaseHrefWebpackPlugin({}),
    new CommonsChunkPlugin({
      name: [
        "inline"
      ],
      minChunks: null
    }),
    new CommonsChunkPlugin({
      name: [
        "vendor"
      ],
      minChunks: (module) => {
        return module.resource
          && (module.resource.startsWith(nodeModules)
            || module.resource.startsWith(genDirNodeModules)
            || module.resource.startsWith(realNodeModules));
      },
      chunks: [
        "main"
      ]
    }),
    new SourceMapDevToolPlugin({
      filename: "[file].map[query]",
      moduleFilenameTemplate: "[resource-path]",
      fallbackModuleFilenameTemplate: "[resource-path]?[hash]",
      sourceRoot: "webpack:///",
      test: /\.(ts|js)($|\?)/i // process .js and .ts files onlytest: /\.(ts|js)($|\?)/i // process .js and .ts files only
    }),
    new CommonsChunkPlugin({
      name: [
        "main"
      ],
      minChunks: 2,
      async: "common"
    }),
    new NamedModulesPlugin({}),
    new AngularCompilerPlugin({
      mainPath: "main.ts",
      platform: 0,
      hostReplacementPaths: {
        "environments\\environment.ts": "environments\\environment.ts"
      },
      sourceMap: true,
      tsConfigPath: "src\\tsconfig.app.json",
      skipCodeGeneration: true,
      compilerOptions: {}
    })
  ],
  node: {
    fs: "empty",
    global: true,
    crypto: "empty",
    tls: "empty",
    net: "empty",
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false
  }
};
