# CapaPlanning

To start your Phoenix server:

  * Install dependencies: `mix deps.get`
  * Install Angular dependencies: `cd lib/capa_planning && npm install`
  * `Docker-compose up -d` to launch your first cockroach database node
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix with `mix start`

To run code analysis:
  * `mix dialyzer`
To run tests :
  * `mix test` (Phoenix)  
  * `npm run test` (Angular)

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
